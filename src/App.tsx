import { Anchor, Center, Stack, Text } from "@mantine/core";
import { Route, Routes } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";

import AppLayout from "./layouts/AppLayout";
import RequireAuth from "./guards/RequireAuth";
import RequireNoAuth from "./guards/RequireNoAuth";

// Import Pages
import LoginPage from "./pages/auth/LoginPage";
import SignupPage from "./pages/auth/SignupPage";
import DashboardPage from "./pages/DashboardPage";
import UsersPage from "./pages/user-management/UsersPage";
import RolesPage from "./pages/user-management/RolesPage";
import AddRolePage from "./pages/user-management/AddRolePage";
import EditRolePage from "./pages/user-management/EditRolePage";
import MyAccountPage from "./pages/my-account/MyAccountPage";
import RouteGuard from "./guards/RouteGuard";
import { PERMISSIONS } from "./guards/permissions";

function App() {
  return (
    <ErrorBoundary FallbackComponent={Fallback} onError={errorHandler}>
      <Routes>
        <Route
          path="/login"
          element={
            <RequireNoAuth>
              <LoginPage />
            </RequireNoAuth>
          }
        />
        <Route
          path="/signup"
          element={
            <RequireNoAuth>
              <SignupPage />
            </RequireNoAuth>
          }
        />
        <Route
          path="/"
          element={
            <RequireAuth>
              <AppLayout />
            </RequireAuth>
          }
        >
          <Route index element={<DashboardPage />} />
          <Route path="my-account" element={<MyAccountPage />} />
          <Route
            path="users"
            element={
              <RouteGuard
                permissions={[PERMISSIONS.USER.MANAGE, PERMISSIONS.USER.READ]}
              >
                <UsersPage />
              </RouteGuard>
            }
          />
          <Route path="roles">
            <Route
              index
              element={
                <RouteGuard
                  permissions={[PERMISSIONS.ROLE.MANAGE, PERMISSIONS.ROLE.READ]}
                >
                  <RolesPage />
                </RouteGuard>
              }
            />
            <Route
              path="add"
              element={
                <RouteGuard
                  permissions={[
                    PERMISSIONS.ROLE.MANAGE,
                    PERMISSIONS.ROLE.CREATE,
                  ]}
                >
                  <AddRolePage />
                </RouteGuard>
              }
            />
            <Route
              path=":id"
              element={
                <RouteGuard
                  permissions={[
                    PERMISSIONS.ROLE.MANAGE,
                    PERMISSIONS.ROLE.UPDATE,
                  ]}
                >
                  <EditRolePage />
                </RouteGuard>
              }
            />
          </Route>
        </Route>
      </Routes>
    </ErrorBoundary>
  );
}

export default App;

const Fallback = () => {
  return (
    <Center style={{ width: "100vw", height: "100vh" }}>
      <Stack>
        <Text weight="bold" size="xl">
          Oh no!!! Something went wrong
        </Text>
        <Anchor href="/">&larr; Back</Anchor>
      </Stack>
    </Center>
  );
};

const errorHandler = (error: Error, errorInfo: any) => {
  console.log("Logging", error, errorInfo);
};
