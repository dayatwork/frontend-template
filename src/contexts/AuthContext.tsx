import {
  createContext,
  FC,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import {
  LoginData,
  me,
  SignupData,
  User,
  login as loginApi,
  signup as signupApi,
  logout as logoutApi,
  clear as clearApi,
} from "../services/auth/auth.service";

type CreateContextTypes = {
  isInitialized: boolean;
  isAuthenticated: boolean;
  signup: (data: SignupData) => Promise<void>;
  login: (data: LoginData) => Promise<void>;
  logout: () => Promise<void>;
  user: User | null;
};

const AuthContext = createContext<CreateContextTypes>({
  isInitialized: false,
  isAuthenticated: false,
  login: async () => {},
  signup: async () => {},
  logout: async () => {},
  user: null,
});

type Props = {
  children: React.ReactNode;
};

export const AuthProvider: FC<Props> = ({ children }) => {
  const isEffectInitialized = useRef(false);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [isInitialized, setIsInitialized] = useState(false);
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    const initialize = async () => {
      try {
        const data = await me();
        setIsInitialized(true);
        setIsAuthenticated(true);
        setUser(data.data);
      } catch (error) {
        setIsInitialized(true);
        setIsAuthenticated(false);
        setUser(null);
      }
    };
    if (!isEffectInitialized.current) {
      isEffectInitialized.current = true;
      initialize();
    }
  }, []);

  const login = async (data: LoginData) => {
    const response = await loginApi(data);
    setIsAuthenticated(true);
    setUser(response.data.user);
  };

  const logout = async () => {
    try {
      await logoutApi();
    } catch (error) {
      await clearApi();
    } finally {
      console.log("logged out");
      setIsAuthenticated(false);
      setUser(null);
    }
  };

  const signup = async (data: SignupData) => {
    const response = await signupApi(data);
    setIsAuthenticated(true);
    setUser(response.data.user);
  };

  return (
    <AuthContext.Provider
      value={{
        isInitialized,
        isAuthenticated,
        login,
        signup,
        logout,
        user,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
