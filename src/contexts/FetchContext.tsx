import { createContext, ReactNode, useEffect, useMemo } from "react";
import axios, { AxiosInstance } from "axios";
import Qs from "qs";

import { useAuth } from "./AuthContext";

export const authAxios = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
  timeout: 5000,
});

const FetchContext = createContext<{ authAxios: AxiosInstance }>({
  authAxios,
});

export const FetchProvider = ({ children }: { children: ReactNode }) => {
  const { logout, isAuthenticated } = useAuth();

  useEffect(() => {
    const requestIntercept = authAxios.interceptors.request.use((config) => {
      config.paramsSerializer = (params) => {
        // Qs is not included in the Axios package
        const newParams = Qs.stringify(params, {
          arrayFormat: "brackets",
          // encode: false,
        });
        return newParams;
      };
      return config;
    });

    const responseIntercept = authAxios.interceptors.response.use(
      (resp) => resp,
      async (error) => {
        // Refresh token if error 401 except for invalid refresh token error
        if (
          error.response.status === 401 &&
          error.response.data.message !== "INVALID_REFRESH_TOKEN" &&
          error.response.data.message !== "NO_REFRESH_TOKEN"
        ) {
          const response = await authAxios.post("/auth/refresh", {});

          if (response.status === 200) {
            return authAxios(error.config);
          }
          // else {
          //   console.log("Logout");
          //   await logout();
          //   return;
          // }
        }

        if (
          error.response.status === 401 &&
          (error.response.data.message !== "INVALID_REFRESH_TOKEN" ||
            error.response.data.message !== "NO_REFRESH_TOKEN")
        ) {
          await logout();
          return;
        }

        // if (
        //   error.response.status === 400 &&
        //   error.response.data.error.message === "Refresh token is missing"
        // ) {
        //   await logout();
        //   return;
        // }

        return Promise.reject(error);
      }
    );

    return () => {
      authAxios.interceptors.request.eject(requestIntercept);
      authAxios.interceptors.response.eject(responseIntercept);
    };
  }, [isAuthenticated, logout]);

  return (
    <FetchContext.Provider value={{ authAxios }}>
      {children}
    </FetchContext.Provider>
  );
};
