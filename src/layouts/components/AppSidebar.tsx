import {
  Navbar,
  ScrollArea,
  createStyles,
  ActionIcon,
  Title,
} from "@mantine/core";
import { useLocalStorage } from "@mantine/hooks";
import {
  Gauge,
  ChevronsRight,
  ChevronsLeft,
  Users,
  UserCheck,
  Icon as TablerIcon,
} from "tabler-icons-react";
import { useLocation } from "react-router-dom";

import { UserButton } from "./UserButton";
import { LinksGroup } from "./NavbarLinksGroup";
import { PERMISSIONS } from "../../guards/permissions";
import { useAuth } from "../../contexts/AuthContext";

type NavItem = {
  label: string;
  icon: TablerIcon;
  link?: string;
  links?: { label: string; link: string; permissions?: string[] }[];
  permissions?: string[];
};

const navigationItems: NavItem[] = [
  { label: "Dashboard", icon: Gauge, link: "/" },
  {
    label: "User Management",
    icon: UserCheck,
    links: [
      {
        label: "Users",
        link: "/users",
        permissions: [PERMISSIONS.USER.MANAGE, PERMISSIONS.USER.READ],
      },
      {
        label: "Roles",
        link: "/roles",
        permissions: [PERMISSIONS.ROLE.MANAGE, PERMISSIONS.ROLE.READ],
      },
    ],
  },
];

const useStyles = createStyles((theme) => ({
  navbar: {
    // backgroundColor:
    //   theme.colorScheme === "dark" ? theme.colors.dark[6] : theme.white,
    backgroundColor: theme.colors[theme.primaryColor][4],
    paddingBottom: 0,
    maxWidth: 260,
    transition: "all 0.1s",
  },

  header: {
    padding: theme.spacing.md,
    paddingTop: 0,
    marginLeft: -theme.spacing.md,
    marginRight: -theme.spacing.md,
    color: theme.colorScheme === "dark" ? theme.white : theme.black,
    borderBottom: `1px solid ${
      theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]
    }`,
  },

  links: {
    marginLeft: -theme.spacing.md,
    marginRight: -theme.spacing.md,
  },

  linksInner: {
    paddingBottom: theme.spacing.xl,
    display: "flex",
    flexDirection: "column",
  },

  footer: {
    marginLeft: -theme.spacing.md,
    marginRight: -theme.spacing.md,
    borderTop: `1px solid ${
      theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]
    }`,
  },
}));

export default function AdminSidebar() {
  const { classes } = useStyles();
  const [mini, setMini] = useLocalStorage({ key: "mini", defaultValue: false });
  const { pathname } = useLocation();
  const { user } = useAuth();

  const links = navigationItems
    .filter((item) =>
      item?.permissions?.length
        ? item.permissions.some((permission) =>
            user?.permissions.includes(permission)
          )
        : true
    )
    .map((item) => (
      <LinksGroup
        mini={mini}
        {...item}
        key={item.label}
        active={pathname === item.link}
        initiallyOpened={pathname.includes(item.label.toLocaleLowerCase())}
      />
    ));

  return (
    <Navbar
      p="md"
      className={classes.navbar}
      style={{
        width: mini ? 67 : 225,
      }}
    >
      <Title align={mini ? "center" : "left"}>{mini ? "L" : "Logo"}</Title>
      <ActionIcon
        mt="md"
        mb="sm"
        variant="filled"
        color="dark"
        size="lg"
        onClick={() => setMini((prev) => !prev)}
      >
        {mini ? <ChevronsRight /> : <ChevronsLeft />}
      </ActionIcon>

      <Navbar.Section grow className={classes.links}>
        {/* component={ScrollArea} */}
        <div className={classes.linksInner}>{links}</div>
      </Navbar.Section>

      <Navbar.Section className={classes.footer}>
        <UserButton
          image="https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=255&q=80"
          name="Dayat"
          email="dayat@mail.com"
          mini={mini}
        />
      </Navbar.Section>
    </Navbar>
  );
}
