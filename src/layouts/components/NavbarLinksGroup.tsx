import { useState } from "react";
import {
  Group,
  Box,
  Collapse,
  ThemeIcon,
  UnstyledButton,
  createStyles,
  Menu,
  Tooltip,
  Text,
} from "@mantine/core";
import {
  Icon as TablerIcon,
  ChevronLeft,
  ChevronRight,
} from "tabler-icons-react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext";

const useStyles = createStyles((theme) => ({
  control: {
    fontWeight: 500,
    display: "block",
    width: "100%",
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,
    color: theme.colorScheme === "dark" ? theme.colors.dark[0] : theme.black,
    fontSize: theme.fontSizes.sm,

    "&:hover": {
      // backgroundColor:
      //   theme.colorScheme === "dark"
      //     ? theme.colors.dark[7]
      //     : theme.colors.gray[0],
      backgroundColor: theme.colors[theme.primaryColor][6],
      color: theme.colorScheme === "dark" ? theme.white : theme.black,
    },
  },

  link: {
    fontWeight: 500,
    display: "block",
    textDecoration: "none",
    padding: `${theme.spacing.xs}px ${theme.spacing.md}px`,
    paddingLeft: 31,
    marginLeft: 30,
    fontSize: theme.fontSizes.sm,
    color: theme.colors.dark[9],
    // borderLeft: `1px solid ${
    //   theme.colorScheme === "dark" ? theme.colors.dark[4] : theme.colors.gray[3]
    // }`,
    borderLeft: `1px solid ${theme.colors[theme.primaryColor][5]}`,

    "&:hover": {
      // backgroundColor:
      //   theme.colorScheme === "dark"
      //     ? theme.colors.dark[7]
      //     : theme.colors.gray[0],
      backgroundColor: theme.colors[theme.primaryColor][6],
      color: theme.colorScheme === "dark" ? theme.white : theme.black,
    },
  },

  active: {
    // "&, &:hover": {
    //   backgroundColor:
    //     theme.colorScheme === "light"
    //       ? theme.colors[theme.primaryColor][0]
    //       : theme.colors[theme.primaryColor][9],
    // },
    backgroundColor: theme.colors.gray[8],
    color: "white",
    "&:hover": {},
  },

  chevron: {
    transition: "transform 200ms ease",
  },
}));

interface LinksGroupProps {
  icon: TablerIcon;
  label: string;
  initiallyOpened?: boolean;
  links?: { label: string; link: string; permissions?: string[] }[];
  mini: boolean;
  link?: string;
  active?: boolean;
}

export function LinksGroup({
  icon: Icon,
  label,
  initiallyOpened,
  links,
  mini,
  link,
  active,
}: LinksGroupProps) {
  const { classes, theme, cx } = useStyles();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const { user } = useAuth();

  const hasLinks = Array.isArray(links);
  const [opened, setOpened] = useState(initiallyOpened || false);
  const ChevronIcon = theme.dir === "ltr" ? ChevronRight : ChevronLeft;
  const items = (hasLinks ? links : [])
    .filter((item) =>
      item?.permissions?.length
        ? item.permissions.some((permission) =>
            user?.permissions.includes(permission)
          )
        : true
    )
    .map((link) => (
      <Link
        style={{ textDecoration: "none" }}
        key={link.label}
        to={link.link}
        className={cx(classes.link, {
          [classes.active]: pathname === link.link,
        })}
      >
        {link.label}
      </Link>
    ));

  if (!mini) {
    return (
      <>
        {hasLinks ? (
          <>
            <UnstyledButton
              onClick={() => setOpened((o) => !o)}
              className={cx(classes.control, { [classes.active]: active })}
            >
              <Group position="apart" spacing={0} noWrap>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                  <ThemeIcon size={34}>
                    <Icon size={22} />
                  </ThemeIcon>
                  <Text ml="sm" style={{ whiteSpace: "nowrap" }}>
                    {label}
                  </Text>
                </Box>
                {hasLinks && (
                  <ChevronIcon
                    className={classes.chevron}
                    size={14}
                    style={{
                      transform: opened
                        ? `rotate(${theme.dir === "rtl" ? -90 : 90}deg)`
                        : "none",
                    }}
                  />
                )}
              </Group>
            </UnstyledButton>
            <Collapse in={opened}>{items}</Collapse>
          </>
        ) : (
          link && (
            <Link
              style={{ textDecoration: "none" }}
              to={link}
              className={cx(classes.control, { [classes.active]: active })}
            >
              {/* <UnstyledButton className={classes.control}> */}
              <Group position="apart" spacing={0}>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                  <ThemeIcon size={34}>
                    <Icon size={22} />
                  </ThemeIcon>
                  <Text ml="sm" style={{ whiteSpace: "nowrap" }}>
                    {label}
                  </Text>
                </Box>
              </Group>
              {/* </UnstyledButton> */}
            </Link>
          )
        )}
      </>
    );
  } else {
    return hasLinks ? (
      <Menu position="right" withArrow opened={opened} onChange={setOpened}>
        <Menu.Target>
          <UnstyledButton>
            <Tooltip
              label={label}
              position="right"
              withArrow
              transitionDuration={0}
              style={{ display: opened ? "none" : "block" }}
            >
              <Group
                position="apart"
                spacing={0}
                className={cx(classes.control, { [classes.active]: active })}
              >
                <Box sx={{ display: "flex", alignItems: "center" }}>
                  <ThemeIcon size={34}>
                    <Icon size={22} />
                  </ThemeIcon>
                </Box>
              </Group>
            </Tooltip>
          </UnstyledButton>
        </Menu.Target>
        <Menu.Dropdown>
          <Menu.Label>User Management</Menu.Label>
          {links?.map((link) => (
            <Menu.Item key={link.link} onClick={() => navigate(link.link)}>
              <Text size="sm">{link.label}</Text>
            </Menu.Item>
          ))}
        </Menu.Dropdown>
      </Menu>
    ) : (
      <Tooltip
        label={label}
        position="right"
        withArrow
        transitionDuration={0}
        style={{ display: "block" }}
      >
        <Link
          to={link ? link : "#"}
          className={cx(classes.control, { [classes.active]: active })}
        >
          <Group position="apart" spacing={0}>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <ThemeIcon variant="light" size={34}>
                <Icon size={22} />
              </ThemeIcon>
            </Box>
          </Group>
        </Link>
      </Tooltip>
    );
  }
}
