import React from "react";
import {
  UnstyledButton,
  Avatar,
  Text,
  createStyles,
  Box,
  Menu,
} from "@mantine/core";
import { ChevronRight } from "tabler-icons-react";
import avatar from "../../assets/avatar.png";
import { useAuth } from "../../contexts/AuthContext";
import { useNavigate } from "react-router-dom";

const useStyles = createStyles((theme) => ({
  user: {
    display: "block",
    width: "100%",
    padding: theme.spacing.md,
    color: theme.colorScheme === "dark" ? theme.colors.dark[0] : theme.black,

    "&:hover": {
      // backgroundColor:
      //   theme.colorScheme === "dark"
      //     ? theme.colors.dark[8]
      //     : theme.colors.gray[0],
      backgroundColor: theme.colors[theme.primaryColor][6],
    },
  },
}));

interface UserButtonProps {
  image: string;
  name: string;
  email: string;
  icon?: React.ReactNode;
  mini: boolean;
}

export function UserButton({
  image,
  name,
  email,
  icon,
  mini,
  ...others
}: UserButtonProps) {
  const navigate = useNavigate();
  const { classes } = useStyles();
  const { user, logout } = useAuth();

  return (
    <Menu
      // style={{ display: "block", width: "100%" }}
      withArrow
      position="right"
    >
      <Menu.Target>
        <UnstyledButton className={classes.user} {...others}>
          <Box style={{ display: "flex" }}>
            <Avatar src={avatar} radius="xl" mr="md" />

            {!mini && (
              <>
                <div style={{ flex: 1 }}>
                  <Text size="sm" weight={600}>
                    {user?.name}
                  </Text>
                  <Text size="xs">{user?.email}</Text>
                </div>

                {icon || <ChevronRight size={14} />}
              </>
            )}
          </Box>
        </UnstyledButton>
      </Menu.Target>
      <Menu.Dropdown>
        <Menu.Item onClick={() => navigate("/my-account")}>
          My Account
        </Menu.Item>
        <Menu.Item onClick={logout}>Logout</Menu.Item>
      </Menu.Dropdown>
    </Menu>
  );
}
