import {
  ActionIcon,
  AppShell,
  Box,
  createStyles,
  Divider,
  Group,
  Header,
  Image,
  Indicator,
  Kbd,
  Switch,
  Text,
  Title,
  useMantineColorScheme,
} from "@mantine/core";
import { Outlet } from "react-router-dom";
import { Bell, MoonStars, Sun } from "tabler-icons-react";
import AppSidebar from "./components/AppSidebar";
// import logo from "../assets/asha-ivf.png";

const useStyles = createStyles((theme) => ({
  root: {
    position: "relative",
    "& *": {
      cursor: "pointer",
    },
  },

  icon: {
    pointerEvents: "none",
    position: "absolute",
    zIndex: 1,
    top: 3,
  },

  iconLight: {
    left: 4,
    color: theme.white,
  },

  iconDark: {
    right: 4,
    color: theme.colors.gray[6],
  },
}));

export default function AppLayout() {
  const { classes, cx } = useStyles();
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  // const dark = colorScheme === "dark";

  const header = (
    <Header
      height={62.5}
      sx={(theme) => ({
        position: "fixed",
        // background: "white",
        width: "100%",
        paddingTop: theme.spacing.xs * 0.8,
        paddingBottom: theme.spacing.xs * 0.8,
        paddingLeft: theme.spacing.lg,
        paddingRight: theme.spacing.xl * 1.5,
        zIndex: 100,
      })}
    >
      <Group mb="xs" position="apart">
        <Title>Logo</Title>
        {/* <Image src={logo} height={50} /> */}
        <Group>
          <Group spacing={4}>
            <Kbd>⌘/ctrl</Kbd>+<Kbd>J</Kbd>
          </Group>
          <Group position="center">
            <div className={classes.root}>
              <Sun className={cx(classes.icon, classes.iconLight)} size={18} />
              <MoonStars
                className={cx(classes.icon, classes.iconDark)}
                size={18}
              />
              <Switch
                checked={colorScheme === "dark"}
                onChange={() => toggleColorScheme()}
                size="md"
              />
            </div>
          </Group>
          <Indicator
            inline
            size={16}
            offset={7}
            position="top-end"
            // color="red"
            withBorder
          >
            <ActionIcon variant="light" size="xl" radius="xl">
              <Bell />
            </ActionIcon>
          </Indicator>
        </Group>
      </Group>
    </Header>
  );

  return (
    <Box style={{ display: "flex" }}>
      <AppSidebar />
      {/* <Box
        sx={(theme) => ({
          flex: 1,
          paddingTop: theme.spacing.xl,
          paddingBottom: theme.spacing.xl,
          paddingLeft: theme.spacing.xl * 1.5,
          paddingRight: theme.spacing.xl * 1.5,
          [`@media (max-width: ${theme.breakpoints.lg}px)`]: {
            paddingLeft: theme.spacing.sm,
            paddingRight: theme.spacing.sm,
          },
        })}
      >
        <Outlet />
      </Box> */}
      <div style={{ flex: 1 }}>
        <Outlet />
      </div>
    </Box>
  );
}
