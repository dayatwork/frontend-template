import { Navigate, useLocation } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";

const RequireAuth = ({ children }: { children: JSX.Element }) => {
  const { isAuthenticated, isInitialized } = useAuth();
  const location = useLocation();
  console.log({ isAuthenticated });

  if (!isInitialized) return null;

  if (!isAuthenticated) {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return children;
};

export default RequireAuth;
