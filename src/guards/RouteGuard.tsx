import { Navigate } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";

type Props = {
  children: JSX.Element;
  permissions?: string[];
  roles?: string[];
  redirectPath?: string;
};

const RouteGuard = ({ children, permissions, roles, redirectPath }: Props) => {
  const { user } = useAuth();
  console.log({ user });

  if (user) {
    if (
      permissions &&
      permissions.length > 0 &&
      !permissions.some((permission) => user.permissions.includes(permission))
    ) {
      return <Navigate to={redirectPath || "/forbidden"} />;
    }

    // if (
    //   roles &&
    //   roles.length > 0 &&
    //   !roles.some((role) => user.roles.includes(role))
    // ) {
    //   return <Navigate to={redirectPath || "/forbidden"} />;
    // }
  }

  return children;
};

export default RouteGuard;
