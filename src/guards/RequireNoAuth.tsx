import { Navigate } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";

const RequireNoAuth = ({ children }: { children: JSX.Element }) => {
  const { user, isInitialized } = useAuth();

  if (!isInitialized) return null;

  if (user) {
    return <Navigate to="/" replace={true} />;
  }

  return children;
};

export default RequireNoAuth;
