import { useAuth } from "../contexts/AuthContext";

type Props = {
  children: JSX.Element;
  permission: string;
};

const ComponentGuard = ({ children, permission }: Props) => {
  const { user } = useAuth();
  const managed = `manage:${permission.split(":")[1]}`;

  if (user) {
    if (
      user.permissions.includes(permission) ||
      user.permissions.includes(managed)
    ) {
      return children;
    }
  }

  return null;
};

export default ComponentGuard;
