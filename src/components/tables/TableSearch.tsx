import { ActionIcon, Group, TextInput } from "@mantine/core";
import { useEffect, useRef, useState } from "react";
import { Search, X } from "tabler-icons-react";

type TableSearchProps = {
  search: string;
  setSearch: React.Dispatch<React.SetStateAction<string>>;
  placeholder?: string;
  label?: string;
  debounce?: number;
  onSearchChange?: () => void;
};

export const TableSearch = ({
  search,
  setSearch,
  label = "Search",
  placeholder = "Search...",
  debounce = 1000,
  onSearchChange,
}: TableSearchProps) => {
  const searchRef = useRef<HTMLInputElement>(null);
  const [value, setValue] = useState(search || "");

  useEffect(() => {
    const delaySetSearch = setTimeout(() => {
      setSearch(value);
      onSearchChange && onSearchChange();
    }, debounce);

    return () => clearTimeout(delaySetSearch);
  }, [value]);

  return (
    <Group>
      <TextInput
        ref={searchRef}
        label={label}
        placeholder={placeholder}
        icon={<Search size={16} />}
        rightSection={
          value ? (
            <ActionIcon
              onClick={() => {
                setSearch("");
                setValue("");
                searchRef.current?.focus();
              }}
            >
              <X size={16} />
            </ActionIcon>
          ) : (
            <div />
          )
        }
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
        }}
      />
    </Group>
  );
};

export default TableSearch;
