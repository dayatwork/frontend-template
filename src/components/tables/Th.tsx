import {
  Center,
  createStyles,
  Group,
  Text,
  UnstyledButton,
} from "@mantine/core";

import { ChevronUp, ChevronDown, Selector } from "tabler-icons-react";

const useStyles = createStyles((theme) => ({
  th: {
    padding: "0 !important",
  },

  sortableControl: {
    width: "100%",
    padding: `${theme.spacing.xs}px ${theme.spacing.xs}px`,
    cursor: "pointer",

    "&:hover": {
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    },
  },

  notSortableControl: {
    width: "100%",
    padding: `${theme.spacing.xs}px ${theme.spacing.xs}px`,
    cursor: "default",
  },

  icon: {
    width: 21,
    height: 21,
    borderRadius: 21,
  },
}));

type ThProps = {
  children: React.ReactNode;
  orderStatus?: "ASC" | "DESC" | "NONE";
  onSort(): void;
  sortable: boolean;
};

export const Th = ({ children, orderStatus, onSort, sortable }: ThProps) => {
  const { classes } = useStyles();
  const Icon =
    orderStatus === "ASC"
      ? ChevronUp
      : orderStatus === "DESC"
      ? ChevronDown
      : Selector;

  return (
    <th className={classes.th}>
      <UnstyledButton
        onClick={onSort}
        className={
          sortable ? classes.sortableControl : classes.notSortableControl
        }
      >
        <Group position="apart">
          <Text weight={500} size="sm">
            {children}
          </Text>
          {sortable && (
            <Center className={classes.icon}>
              <Icon size={14} />
            </Center>
          )}
        </Group>
      </UnstyledButton>
    </th>
  );
};

export default Th;
