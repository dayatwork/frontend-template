import { Skeleton } from "@mantine/core";

type TableSkeletonProps = {
  columns: number;
  rows: number;
};

export const TableSkeleton = ({ columns, rows }: TableSkeletonProps) => {
  return (
    <>
      {Array.from({ length: rows }).map((_, i) => {
        return (
          <tr key={i}>
            {Array.from({ length: columns }).map((_, j) => {
              return (
                <td key={j} style={{ paddingTop: 16, paddingBottom: 16 }}>
                  <Skeleton height={10} />
                </td>
              );
            })}
          </tr>
        );
      })}
    </>
  );
};

export default TableSkeleton;
