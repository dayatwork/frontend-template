import { Group, Pagination, Text } from "@mantine/core";

import { MetaType } from "../../services/types";

type TablePaginationBottomProps = {
  meta?: MetaType;
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
};

export const TablePaginationBottom = ({
  meta,
  page,
  setPage,
}: TablePaginationBottomProps) => {
  return (
    <Group mt="md" position="right">
      <Text>
        {(meta?.currentPage || 1) - 1 + 1}-
        {(meta?.currentPage || 1) * (meta?.itemsPerPage || 1) >
        (meta?.totalItems || 1)
          ? meta?.totalItems
          : (meta?.currentPage || 1) * (meta?.itemsPerPage || 1)}{" "}
        of {meta?.totalItems}
      </Text>
      <Pagination
        page={page}
        onChange={setPage}
        total={meta?.totalPages || 1}
      />
    </Group>
  );
};

export default TablePaginationBottom;
