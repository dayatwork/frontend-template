import { useState } from "react";
import {
  ActionIcon,
  Button,
  Center,
  Group,
  MultiSelect,
  NumberInput,
  Popover,
  Select,
  SimpleGrid,
  Text,
  TextInput,
  Tooltip,
} from "@mantine/core";
import { Filter, InfoCircle, X } from "tabler-icons-react";
import { DatePicker, DateRangePicker } from "@mantine/dates";
import { format } from "date-fns";

type TextValue = {
  type: "text";
};

type NumberValue = {
  type: "number";
};

type NumberRangeValue = {
  type: "number-range";
};

type DateValue = {
  type: "date";
};

type DateRangeValue = {
  type: "date-range";
};

type SelectValue = {
  type: "select";
  options: {
    label: string;
    value: string;
  }[];
};

type MultiSelectValue = {
  type: "multi-select";
  options: {
    label: string;
    value: string;
  }[];
};

type EqualField = {
  name: string;
  displayName: string;
  operator: "$eq";
  value: TextValue | NumberValue | DateValue | SelectValue;
};

type RangeField = {
  name: string;
  displayName: string;
  operator: "$btw";
  value: NumberRangeValue | DateRangeValue;
};

type MultipleField = {
  name: string;
  displayName: string;
  operator: "$in";
  value: MultiSelectValue;
};

type Props = {
  fields: (EqualField | RangeField | MultipleField)[];
  filter: Record<string, string>;
  setFilter: React.Dispatch<React.SetStateAction<Record<string, string>>>;
};

export const TableFilter = ({ fields, filter, setFilter }: Props) => {
  const [filterOpened, setFilterOpened] = useState(false);
  const [filtr, setFiltr] = useState<
    Record<string, { operator: string | null; type: string; value: any }>
  >(
    fields.reduce(
      (acc, f) => ({
        ...acc,
        [f.name]: {
          operator: f.operator,
          value: filter[f.name]
            ? formatStringToValue(f.value.type, filter[f.name])
            : "",
        },
      }),
      {}
    )
  );

  const onClear = () => {
    setFilter({});
    setFiltr(
      fields.reduce(
        (acc, f) => ({
          ...acc,
          [f.name]: {
            operator: f.operator,
            value: "",
          },
        }),
        {}
      )
    );
  };

  const onFilter = () => {
    // Filter if there is a value and an operator for each field, and format the filter
    const filteredField = Object.entries(filtr)
      .filter(
        (item) =>
          item[1].value &&
          item[1].operator &&
          (item[1].type === "multi-select"
            ? item[1].value.length > 0
            : item[1].value)
      )
      .reduce(
        (a, v) => ({
          ...a,
          [v[0]]: `${v[1].operator}:${formatValue(v[1].type, v[1].value)}`,
        }),
        {}
      );

    setFilter(filteredField);
    setFilterOpened(false);
  };

  return (
    <Button.Group>
      {/* Filter Popover */}
      <Popover
        opened={filterOpened}
        onClose={() => setFilterOpened(false)}
        width={650}
        withArrow
        position="bottom"
        closeOnClickOutside={false}
      >
        <Popover.Target>
          <ActionIcon
            variant="light"
            size={36}
            sx={(theme) => ({
              border: "1px solid",
              borderColor: theme.colors.gray[4],
            })}
            onClick={() => setFilterOpened((o) => !o)}
          >
            <Filter />
          </ActionIcon>
        </Popover.Target>
        <Popover.Dropdown sx={(theme) => ({ boxShadow: theme.shadows.xl })}>
          <Text weight={600} mb="xs">
            Filter
          </Text>
          <SimpleGrid cols={2}>
            {fields.map((field) => (
              <div key={field.name}>
                {field.value.type === "text" && (
                  <TextInput
                    label={field.displayName}
                    placeholder="Value..."
                    value={filtr[field.name]?.value}
                    onChange={(e) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: e.target.value,
                        },
                      });
                    }}
                    rightSection={
                      <Tooltip
                        label="Must exactly match the value to be searched. Use search fields to search for partial matches."
                        position="top-end"
                        withArrow
                        transition="pop-bottom-right"
                      >
                        <Text color="dimmed" sx={{ cursor: "help" }}>
                          <Center>
                            <InfoCircle size={18} />
                          </Center>
                        </Text>
                      </Tooltip>
                    }
                  />
                )}
                {field.value.type === "number" && (
                  <NumberInput
                    label={field.displayName}
                    placeholder="Value..."
                    value={filtr[field.name]?.value}
                    onChange={(v) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: v,
                        },
                      });
                    }}
                  />
                )}
                {field.value.type === "number-range" && (
                  <SimpleGrid cols={2}>
                    <NumberInput
                      label={`${field.displayName} (from)`}
                      placeholder="Value..."
                      value={filtr[field.name]?.value}
                      onChange={(v) => {
                        setFiltr({
                          ...filtr,
                          [field.name]: {
                            operator: filtr[field.name].operator,
                            type: field.value.type,
                            value: [v, filtr[field.name].value[1]],
                          },
                        });
                      }}
                    />
                    <NumberInput
                      label={`${field.displayName} (to)`}
                      placeholder="Value..."
                      value={filtr[field.name]?.value}
                      onChange={(v) => {
                        setFiltr({
                          ...filtr,
                          [field.name]: {
                            operator: filtr[field.name].operator,
                            type: field.value.type,
                            value: [filtr[field.name].value[0], v],
                          },
                        });
                      }}
                    />
                  </SimpleGrid>
                )}
                {field.value.type === "select" && (
                  <Select
                    clearable
                    searchable
                    label={field.displayName}
                    placeholder="Value..."
                    data={field.value.options}
                    value={filtr[field.name]?.value}
                    onChange={(v) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: v,
                        },
                      });
                    }}
                  />
                )}
                {field.value.type === "multi-select" && (
                  <MultiSelect
                    clearable
                    searchable
                    label={field.displayName}
                    placeholder="Value..."
                    data={field.value.options}
                    value={filtr[field.name]?.value || []}
                    onChange={(v) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: v,
                        },
                      });
                    }}
                  />
                )}
                {field.value.type === "date" && (
                  <DatePicker
                    clearable
                    label={field.displayName}
                    placeholder="Value..."
                    value={filtr[field.name]?.value}
                    onChange={(v) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: v,
                        },
                      });
                    }}
                  />
                )}
                {field.value.type === "date-range" && (
                  <DateRangePicker
                    clearable
                    dropdownPosition="bottom-start"
                    label={field.displayName}
                    placeholder="Value..."
                    value={filtr[field.name]?.value}
                    onChange={(v) => {
                      setFiltr({
                        ...filtr,
                        [field.name]: {
                          operator: filtr[field.name].operator,
                          type: field.value.type,
                          value: v,
                        },
                      });
                    }}
                  />
                )}
              </div>
            ))}
          </SimpleGrid>
          <Group spacing={4} mt="xl" position="right">
            <Button
              variant="subtle"
              color="gray"
              onClick={() => setFilterOpened(false)}
            >
              Close
            </Button>
            <Button pl="xs" leftIcon={<Filter />} onClick={onFilter}>
              Filter
            </Button>
          </Group>
        </Popover.Dropdown>
      </Popover>
      {/* Clear button */}
      {Object.keys(filter).length > 0 && (
        <Tooltip label="Clear Filter">
          <ActionIcon
            size={36}
            sx={(theme) => ({
              border: "1px solid",
              borderColor: theme.colors.gray[4],
            })}
            onClick={onClear}
          >
            <X size={20} />
          </ActionIcon>
        </Tooltip>
      )}
    </Button.Group>
  );
};

export default TableFilter;

const formatValue = (type: string, value: any) => {
  switch (type) {
    case "text":
      return value;
    case "number":
      return value;
    case "select":
      return value;
    case "multi-select":
      return value.join(",");
    case "number-range":
      return `${value[0]},${value[1]}`;
    case "date":
      return format(value, "yyyy-MM-dd");
    case "date-range":
      return `${format(value[0], "yyyy-MM-dd")},${format(
        value[1],
        "yyyy-MM-dd"
      )}`;
    default:
      return value;
  }
};

const formatStringToValue = (type: string, value: string) => {
  switch (type) {
    case "text":
      return value;
    case "number":
      return value;
    case "select":
      return value;
    case "multi-select":
      return value.split(",");
    case "number-range":
      return value.split(",").map((v) => parseInt(v));
    case "date":
      return new Date(value);
    case "date-range":
      return value.split(",").map((v) => new Date(v));
    default:
      return value;
  }
};
