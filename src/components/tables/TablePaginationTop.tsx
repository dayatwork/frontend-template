import { ActionIcon, Group, NumberInput, Select, Tooltip } from "@mantine/core";
import { ChevronLeft, ChevronRight, Refresh } from "tabler-icons-react";

import { MetaType } from "../../services/types";

type TablePaginationTopProps = {
  meta?: MetaType;
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
  limit: string;
  setLimit: React.Dispatch<React.SetStateAction<string>>;
  refetch: () => void;
  isRefetching: boolean;
};

export const TablePaginationTop = ({
  page,
  setPage,
  meta,
  limit,
  setLimit,
  refetch,
  isRefetching,
}: TablePaginationTopProps) => {
  return (
    <Group align="flex-end">
      <NumberInput
        label="Page"
        sx={{ width: 80 }}
        min={1}
        max={meta?.totalPages || 1}
        value={page}
        onChange={(v) => v && setPage(v)}
      />
      <Select
        label="Show"
        sx={{ width: 80 }}
        data={["5", "10", "25", "50", "100", "All"]}
        value={limit}
        onChange={(v) => {
          setPage(1);
          v && setLimit(v);
        }}
      />
      <Group spacing={8}>
        <Tooltip label="Previous page">
          <ActionIcon
            sx={(theme) => ({
              border: "1px solid",
              borderColor: theme.colors.gray[4],
              width: 36,
              height: 36,
            })}
            disabled={page <= 1}
            onClick={() => setPage((prev) => prev - 1)}
          >
            <ChevronLeft size={20} />
          </ActionIcon>
        </Tooltip>
        <Tooltip label="Next page">
          <ActionIcon
            sx={(theme) => ({
              border: "1px solid",
              borderColor: theme.colors.gray[4],
              width: 36,
              height: 36,
            })}
            disabled={page >= (meta?.totalPages || 1)}
            onClick={() => setPage((prev) => prev + 1)}
          >
            <ChevronRight size={20} />
          </ActionIcon>
        </Tooltip>
        <Tooltip label="Refresh">
          <ActionIcon
            sx={(theme) => ({
              border: "1px solid",
              borderColor: theme.colors.gray[4],
              width: 36,
              height: 36,
            })}
            onClick={refetch}
            loading={isRefetching}
          >
            <Refresh size={20} />
          </ActionIcon>
        </Tooltip>
      </Group>
    </Group>
  );
};

export default TablePaginationTop;
