import { Center, Text } from "@mantine/core";

type TableEmptyProps = {
  columns: number;
};

export const TableEmpty = ({ columns }: TableEmptyProps) => {
  return (
    <tr>
      <td colSpan={columns}>
        <Center py={20}>
          <Text>Data Not Found</Text>
        </Center>
      </td>
    </tr>
  );
};

export default TableEmpty;
