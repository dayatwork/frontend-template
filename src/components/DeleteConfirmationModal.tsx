import { Button, Group, LoadingOverlay, Modal, Text } from "@mantine/core";
import React, { useState } from "react";

type Props = {
  title?: string;
  confirmationString?: string;
  opened: boolean;
  setOpened: React.Dispatch<React.SetStateAction<boolean>>;
  onConfirm: () => Promise<any>;
  onCancel?: () => void;
  cancelString?: string;
  confirmString?: string;
};

const DeleteConfirmationModal = ({
  onCancel,
  onConfirm,
  opened,
  setOpened,
  title = "Delete",
  confirmationString = "Are you sure you want to delete this data?",
  cancelString = "Cancel",
  confirmString = "Confirm",
}: Props) => {
  const [loading, setLoading] = useState(false);

  const handleConfirm = async () => {
    setLoading(true);
    await onConfirm();
    setLoading(false);
    setOpened(false);
  };

  const handleCancel = () => {
    setOpened(false);
    onCancel?.();
  };

  return (
    <Modal opened={opened} onClose={() => setOpened(false)} title={title}>
      <div style={{ position: "relative" }}>
        <LoadingOverlay visible={loading} />
        <Text>{confirmationString}</Text>
        <Group mt="xl" position="right">
          <Button onClick={handleCancel} variant="subtle" color="dark">
            {cancelString}
          </Button>
          <Button onClick={handleConfirm} color="red">
            {confirmString}
          </Button>
        </Group>
      </div>
    </Modal>
  );
};

export default DeleteConfirmationModal;
