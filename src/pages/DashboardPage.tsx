import {
  Box,
  createStyles,
  Group,
  Paper,
  SimpleGrid,
  Text,
  Title,
} from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { Calendar } from "tabler-icons-react";

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors[theme.primaryColor][6],
    height: "100%",
  },
  titleContainer: {
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    paddingTop: theme.spacing.xl * 1.2,
    paddingBottom: theme.spacing.xl * 1.2,
    backgroundColor: theme.colors[theme.primaryColor][1],
  },
  title: {
    fontSize: theme.fontSizes.xl * 1.5,
  },
  statBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.lg,
    backgroundColor: theme.colors[theme.primaryColor][1],
    boxShadow: theme.shadows.xs,
    // maxWidth: 250,
  },
  queueBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.lg,
    backgroundColor: theme.colors.green[1],
    boxShadow: theme.shadows.xs,
    // maxWidth: 250,
  },
  closeQueue: {
    backgroundColor: theme.colors.gray[1],
  },
  label: {
    fontSize: theme.fontSizes.xl,
    fontWeight: 500,
  },
  value: {
    fontSize: theme.fontSizes.xl * 1.5,
    fontWeight: 600,
  },
}));

const stats = [
  {
    label: "Total Pasien",
    value: 300,
  },
  {
    label: "Pasien Baru",
    value: 150,
  },
  {
    label: "Pasien Lama",
    value: 150,
  },
  {
    label: "Total Booking",
    value: 25,
  },
  {
    label: "Booking Baru",
    value: 20,
  },
  {
    label: "Cancel",
    value: 3,
  },
  {
    label: "Total Checkin",
    value: 15,
  },
  {
    label: "Total Checkout",
    value: 17,
  },
  {
    label: "Total Referral Dokter",
    value: 15,
  },
];

const queues = [
  {
    label: "Nomor Antrian Admisi",
    value: 14,
    open: true,
  },
  {
    label: "Nomor Antrian Konsultasi",
    value: 10,
    open: true,
  },
  {
    label: "Nomor Antrian Laboratorium",
    value: 5,
    open: false,
  },
  {
    label: "Nomor Antrian Kasir",
    value: 8,
    open: true,
  },
];

const DashboardPage = () => {
  const { classes, cx } = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.titleContainer}>
        <Title className={classes.title}>Dashboard</Title>
      </Box>
      <Group pt="sm" px="xl" position="right">
        <DatePicker
          icon={<Calendar size={18} />}
          label="Date"
          value={new Date()}
        />
      </Group>
      <Box p="xl">
        <SimpleGrid cols={2}>
          <SimpleGrid cols={3}>
            {stats.map((stat) => (
              <Paper key={stat.label} className={classes.statBox}>
                <Text className={classes.label}>{stat.label}</Text>
                <Text className={classes.value}>{stat.value}</Text>
              </Paper>
            ))}
          </SimpleGrid>

          <SimpleGrid cols={2}>
            {queues.map((queue) => (
              <Paper
                key={queue.label}
                className={cx(classes.queueBox, {
                  [classes.closeQueue]: !queue.open,
                })}
              >
                <Text className={classes.label}>{queue.label}</Text>
                <Text className={classes.value}>{queue.value}</Text>
              </Paper>
            ))}
          </SimpleGrid>
        </SimpleGrid>
      </Box>
    </Box>
  );
};

export default DashboardPage;
