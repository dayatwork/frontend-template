import {
  ActionIcon,
  Box,
  Button,
  Checkbox,
  createStyles,
  Group,
  Paper,
  SimpleGrid,
  Stack,
  Text,
  Textarea,
  TextInput,
  Title,
  useMantineTheme,
} from "@mantine/core";
import { useInputState } from "@mantine/hooks";
import { showNotification } from "@mantine/notifications";
import { useQueryClient } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { ArrowLeft } from "tabler-icons-react";
import {
  ROLE_KEY,
  useAvailablePermissions,
  useRole,
} from "../../services/roles/roles.hook";
import { editRole } from "../../services/roles/roles.service";

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors[theme.primaryColor][6],
    height: "100%",
  },
  titleContainer: {
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    height: 80,
    backgroundColor: theme.colors[theme.primaryColor][1],
  },
  bodyContainer: {
    height: "calc(100% - 80px)",
    padding: theme.spacing.lg,
  },
  title: {
    fontSize: theme.fontSizes.xl * 1.5,
  },
}));

const EditRolePage = () => {
  const params = useParams<{ id: string }>();
  const theme = useMantineTheme();
  const { classes } = useStyles();
  const [name, setName] = useInputState("");
  const [description, setDescription] = useInputState("");
  const [displayName, setDisplayName] = useInputState("");
  const [permissions, setPermissions] = useState<string[]>([]);
  const queryClient = useQueryClient();

  const { data: dataAvailablePermissions } = useAvailablePermissions();
  const availablePermissions = dataAvailablePermissions?.data || [];
  const { data: dataRole } = useRole(params.id || "");

  useEffect(() => {
    if (dataRole?.data) {
      setName(dataRole.data.name);
      setDescription(dataRole.data.description);
      setDisplayName(dataRole.data.display_name);
      setPermissions(dataRole.data.permissions);
    }
  }, [dataRole?.data]);

  const handleChangePermissions = (v: string[]) => {
    const managedResources = v
      .filter((p) => p.split(":")[0] === "manage")
      .map((p) => p.split(":")[1]);

    const filteredPermissions = v.filter((p) => {
      return (
        p.split(":")[0] === "manage" ||
        !managedResources.includes(p.split(":")[1])
      );
    });

    setPermissions(filteredPermissions);
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const id = params.id || "";

    if (!id || !Number(id)) return;

    try {
      const response = await editRole(Number(id), {
        name,
        description,
        display_name: displayName,
        permissions,
      });
      queryClient.invalidateQueries([ROLE_KEY]);
      showNotification({
        title: "Role updated",
        message: `Role ${response.data.name} updated successfully`,
        color: "green",
      });
    } catch (error: any) {
      showNotification({
        title: "Error",
        message: error.message,
        color: "red",
      });
    }
  };

  return (
    <Box className={classes.root}>
      <Group align="center" className={classes.titleContainer}>
        <ActionIcon component={Link} to="/roles" color={theme.primaryColor}>
          <ArrowLeft />
        </ActionIcon>
        <Title order={2}>Edit Role</Title>
      </Group>

      <Box className={classes.bodyContainer}>
        <Paper
          withBorder
          p="md"
          sx={(theme) => ({ width: theme.breakpoints.sm })}
        >
          <form onSubmit={handleSubmit}>
            <Stack>
              <SimpleGrid cols={2}>
                <TextInput label="Name" value={name} onChange={setName} />
                <TextInput
                  label="Display Name"
                  value={displayName}
                  onChange={setDisplayName}
                />
              </SimpleGrid>
              <Textarea
                label="Description"
                value={description}
                onChange={setDescription}
              />
              <Checkbox.Group
                label="Permissions"
                value={permissions}
                onChange={handleChangePermissions}
              >
                <Paper withBorder p="sm" sx={{ width: "100%" }}>
                  <Stack>
                    {availablePermissions.map((group) => (
                      <div key={group.resource}>
                        <Title
                          order={4}
                          mb={4}
                          sx={(theme) => ({
                            fontSize: theme.fontSizes.md,
                            fontWeight: 500,
                            textTransform: "capitalize",
                          })}
                        >
                          {group.resource}
                        </Title>
                        <Group>
                          {group.permissions.map((permission) => (
                            <Checkbox
                              key={permission}
                              label={
                                <Text transform="capitalize">
                                  {permission.split(":")[0].replace("-", " ")}
                                </Text>
                              }
                              disabled={
                                permissions.includes(
                                  `manage:${permission.split(":")[1]}`
                                ) &&
                                permission !==
                                  `manage:${permission.split(":")[1]}`
                              }
                              value={permission}
                              checked={true}
                            />
                          ))}
                        </Group>
                      </div>
                    ))}
                  </Stack>
                </Paper>
              </Checkbox.Group>
            </Stack>
            <Group position="right" mt="lg">
              <Button type="submit">Save</Button>
            </Group>
          </form>
        </Paper>
      </Box>
    </Box>
  );
};

export default EditRolePage;
