import {
  Button,
  Group,
  LoadingOverlay,
  Modal,
  MultiSelect,
} from "@mantine/core";
import { useQueryClient } from "@tanstack/react-query";
import React, { useEffect, useState } from "react";
import { useRoles } from "../../../services/roles/roles.hook";
import { USER_KEY, useUser } from "../../../services/users/users.hook";
import { setUserRoles } from "../../../services/users/users.service";

type Props = {
  opened: boolean;
  setOpened: React.Dispatch<React.SetStateAction<boolean>>;
  user: {
    id: string;
    roles: {
      id: number;
    }[];
  };
};

const SetUserRoleModal = ({ opened, setOpened, user }: Props) => {
  const [roles, setRoles] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const queryClient = useQueryClient();

  const { data: dataRoles } = useRoles({});

  useEffect(() => {
    if (user) {
      setRoles(user?.roles?.map((role) => String(role.id)));
    }
  }, [user]);

  const selectOptions =
    dataRoles?.data?.map((item) => ({
      label: item.display_name,
      value: String(item.id),
    })) || [];

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const newRoles = roles.map((item) => Number(item));

    try {
      setIsLoading(true);
      await setUserRoles({ rolesIds: newRoles, userId: user.id });
      queryClient.invalidateQueries([USER_KEY]);
      setIsLoading(false);
      setOpened(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  return (
    <Modal
      opened={opened}
      onClose={() => setOpened(false)}
      title="Set User Roles"
    >
      <form onSubmit={onSubmit} style={{ position: "relative" }}>
        <LoadingOverlay visible={isLoading} />
        <MultiSelect data={selectOptions} value={roles} onChange={setRoles} />

        <Group position="right" mt="lg">
          <Button type="submit">Save</Button>
        </Group>
      </form>
    </Modal>
  );
};

export default SetUserRoleModal;
