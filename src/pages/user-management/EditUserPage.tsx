import {
  ActionIcon,
  Button,
  Checkbox,
  Group,
  Input,
  Paper,
  SimpleGrid,
  Stack,
  Text,
  Textarea,
  TextInput,
  Title,
  useMantineTheme,
} from "@mantine/core";
import { useInputState } from "@mantine/hooks";
import { showNotification } from "@mantine/notifications";
import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { ArrowLeft, Check } from "tabler-icons-react";
import {
  useAvailablePermissions,
  useRole,
} from "../../services/roles/roles.hook";
import { createRole, editRole } from "../../services/roles/roles.service";

const EditUserPage = () => {
  const navigate = useNavigate();
  const theme = useMantineTheme();
  const params = useParams<{ id: string }>();
  const [name, setName] = useInputState("");
  const [description, setDescription] = useInputState("");
  const [displayName, setDisplayName] = useInputState("");
  const [permissions, setPermissions] = useState<string[]>([]);

  const { data: dataAvailablePermissions } = useAvailablePermissions();
  const availablePermissions = dataAvailablePermissions?.data || [];
  const { data: dataRole } = useRole(params.id || "");

  useEffect(() => {
    if (dataRole?.data) {
      setName(dataRole.data.name);
      setDescription(dataRole.data.description);
      setDisplayName(dataRole.data.display_name);
      setPermissions(dataRole.data.permissions);
    }
  }, [dataRole?.data]);

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const id = params.id || "";

    if (!id || !Number(id)) return;

    try {
      const response = await editRole(Number(id), {
        name,
        description,
        display_name: displayName,
        permissions,
      });
      showNotification({
        title: "Role updated",
        message: `Role ${response.data.name} updated successfully`,
        color: "green",
      });
    } catch (error: any) {
      showNotification({
        title: "Error",
        message: error.message,
        color: "red",
      });
    }
  };

  return (
    <>
      <Group align="center">
        <ActionIcon component={Link} to="/roles" color={theme.primaryColor}>
          <ArrowLeft />
        </ActionIcon>
        <Title order={2}>Edit Role</Title>
      </Group>

      <Paper
        mt="md"
        withBorder
        p="md"
        sx={(theme) => ({ width: theme.breakpoints.sm })}
      >
        <form onSubmit={handleSubmit}>
          <Stack>
            <SimpleGrid cols={2}>
              <TextInput label="Name" value={name} onChange={setName} />
              <TextInput
                label="Display Name"
                value={displayName}
                onChange={setDisplayName}
              />
            </SimpleGrid>
            <Textarea
              label="Description"
              value={description}
              onChange={setDescription}
            />
            <Checkbox.Group
              label="Permissions"
              value={permissions}
              onChange={(v) => {
                const managedResources = v
                  .filter((p) => p.split(":")[0] === "manage")
                  .map((p) => p.split(":")[1]);

                const filteredPermissions = v.filter((p) => {
                  return (
                    p.split(":")[0] === "manage" ||
                    !managedResources.includes(p.split(":")[1])
                  );
                });

                setPermissions(filteredPermissions);
              }}
            >
              <Paper withBorder p="sm" sx={{ width: "100%" }}>
                <Stack>
                  {availablePermissions.map((group) => (
                    <div key={group.resource}>
                      <Title
                        order={4}
                        mb={4}
                        sx={(theme) => ({
                          fontSize: theme.fontSizes.md,
                          fontWeight: 500,
                          textTransform: "capitalize",
                        })}
                      >
                        {group.resource}
                      </Title>
                      <Group>
                        {group.permissions.map((permission) => (
                          <Checkbox
                            key={permission}
                            label={
                              <Text transform="capitalize">
                                {permission.split(":")[0].replace("-", " ")}
                              </Text>
                            }
                            disabled={
                              permissions.includes(
                                `manage:${permission.split(":")[1]}`
                              ) &&
                              permission !==
                                `manage:${permission.split(":")[1]}`
                            }
                            value={permission}
                            checked={true}
                          />
                        ))}
                      </Group>
                    </div>
                  ))}
                </Stack>
              </Paper>
            </Checkbox.Group>
          </Stack>
          <Group position="right" mt="lg">
            <Button type="submit">Save</Button>
          </Group>
        </form>
      </Paper>
    </>
  );
};

export default EditUserPage;
