import {
  ActionIcon,
  Box,
  Button,
  Container,
  createStyles,
  Group,
  Paper,
  Table,
  Title,
  Tooltip,
} from "@mantine/core";
import { useState } from "react";
import { Edit, Trash, UserCheck } from "tabler-icons-react";
import {
  TableEmpty,
  TableFilter,
  TablePaginationBottom,
  TablePaginationTop,
  TableSearch,
  TableSkeleton,
  Th,
} from "../../components/tables";
import ComponentGuard from "../../guards/ComponentGuard";
import { PERMISSIONS } from "../../guards/permissions";
import { useUsers } from "../../services/users/users.hook";
import SetUserRoleModal from "./components/SetUserRoleModal";

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors[theme.primaryColor][6],
    height: "100%",
  },
  titleContainer: {
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    height: 80,
    backgroundColor: theme.colors[theme.primaryColor][1],
  },
  bodyContainer: {
    height: "calc(100% - 80px)",
    padding: theme.spacing.lg,
  },
  title: {
    fontSize: theme.fontSizes.xl * 1.5,
  },
}));

const DEFAULT_SORT: Record<string, "NONE" | "ASC" | "DESC"> = {
  name: "NONE",
  email: "NONE",
  phone: "NONE",
  dob: "NONE",
  gender: "NONE",
  status: "NONE",
};

const UsersPage = () => {
  const { classes } = useStyles();
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState("5");
  const [search, setSearch] = useState("");
  const [filter, setFilter] = useState<Record<string, string>>({});
  const [sort, setSort] =
    useState<Record<string, "NONE" | "ASC" | "DESC">>(DEFAULT_SORT);

  const [isSetUserRoleOpen, setIsSetUserRoleOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState<any>(null);

  const sortQuery = Object.entries(sort)
    .filter((item) => item[1] !== "NONE")
    .map((item) => `${item[0]}:${item[1]}`)
    .join(",");

  const {
    data: users,
    isLoading,
    isRefetching,
    refetch,
  } = useUsers({
    limit: limit === "All" ? undefined : +limit,
    page: page,
    search: search ? search : undefined,
    filter: filter,
    sortBy: sortQuery ? sortQuery : undefined,
  });

  const columns = [
    {
      name: "No.",
      field: "id",
      sortable: false,
    },
    {
      name: "Name",
      field: "name",
      sortable: true,
    },
    {
      name: "Email",
      field: "email",
      sortable: true,
    },
    {
      name: "Phone",
      field: "phone",
      sortable: true,
    },
    {
      name: "Role",
      field: "role",
      sortable: false,
    },
    {
      name: "Action",
      field: "action",
      sortable: false,
    },
  ];

  const onSort = (field: string) => {
    const sortField = sort[field];
    if (sortField === "NONE") {
      setSort({ ...sort, [field]: "ASC" });
    } else if (sortField === "ASC") {
      setSort({ ...sort, [field]: "DESC" });
    } else {
      setSort({ ...sort, [field]: "NONE" });
    }
  };
  const isTableSorted = JSON.stringify(sort) !== JSON.stringify(DEFAULT_SORT);

  const rows = users?.data?.map((u, index) => (
    <tr key={u.id}>
      <td style={{ paddingLeft: 16 }}>
        {(users.meta.currentPage - 1) * users.meta.itemsPerPage + index + 1}
      </td>
      <td>{u.name}</td>
      <td>{u.email}</td>
      <td>{u.phone}</td>
      <td>{u.roles?.map((r) => r.display_name).join(", ")}</td>
      <td>
        <Group spacing={4}>
          <ComponentGuard permission={PERMISSIONS.USER.SET_ROLE}>
            <Tooltip label="Roles">
              <ActionIcon
                color="green"
                onClick={() => {
                  setSelectedUser(u);
                  setIsSetUserRoleOpen(true);
                }}
              >
                <UserCheck size={16} />
              </ActionIcon>
            </Tooltip>
          </ComponentGuard>
          <ComponentGuard permission={PERMISSIONS.USER.UPDATE}>
            <Tooltip label="Edit">
              <ActionIcon color="blue">
                <Edit size={16} />
              </ActionIcon>
            </Tooltip>
          </ComponentGuard>
          <ComponentGuard permission={PERMISSIONS.USER.DELETE}>
            <Tooltip label="Delete">
              <ActionIcon color="red">
                <Trash size={16} />
              </ActionIcon>
            </Tooltip>
          </ComponentGuard>
        </Group>
      </td>
    </tr>
  ));

  return (
    <Box className={classes.root}>
      <SetUserRoleModal
        opened={isSetUserRoleOpen}
        setOpened={setIsSetUserRoleOpen}
        user={selectedUser}
      />
      <Group position="apart" align="center" className={classes.titleContainer}>
        <Title order={2} className={classes.title}>
          Users
        </Title>
        <ComponentGuard permission={PERMISSIONS.USER.CREATE}>
          <Button>Add User</Button>
        </ComponentGuard>
      </Group>
      <Box className={classes.bodyContainer}>
        <Box
          p="md"
          sx={{ backgroundColor: "white", borderRadius: 6, height: "100%" }}
        >
          <Paper withBorder mb="xs" p="sm">
            <Group position="apart">
              <Group align="flex-end">
                <TableSearch
                  search={search}
                  setSearch={setSearch}
                  placeholder="Name or email or phone"
                  debounce={400}
                  onSearchChange={() => page !== 1 && setPage(1)}
                />
                <TableFilter
                  fields={[
                    {
                      name: "name",
                      displayName: "Name",
                      operator: "$eq",
                      value: {
                        type: "text",
                      },
                    },
                    {
                      name: "email",
                      displayName: "Email",
                      operator: "$eq",
                      value: {
                        type: "text",
                      },
                    },
                    {
                      name: "phone",
                      displayName: "Phone",
                      operator: "$eq",
                      value: {
                        type: "text",
                      },
                    },
                  ]}
                  filter={filter}
                  setFilter={setFilter}
                />
                <Group spacing="xs">
                  {isTableSorted && (
                    <Button
                      variant="subtle"
                      onClick={() => setSort(DEFAULT_SORT)}
                    >
                      Clear Sort
                    </Button>
                  )}
                </Group>
              </Group>
              <TablePaginationTop
                limit={limit}
                setLimit={setLimit}
                page={page}
                setPage={setPage}
                refetch={refetch}
                isRefetching={isRefetching}
                meta={users?.meta}
              />
            </Group>
          </Paper>
          <Paper withBorder>
            <Table>
              <thead>
                <tr>
                  {columns.map((h) => (
                    <Th
                      key={h.field}
                      onSort={() => onSort(h.field)}
                      orderStatus={sort[h.field]}
                      sortable={h.sortable}
                    >
                      {h.name}
                    </Th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {isLoading ? (
                  <TableSkeleton columns={columns.length} rows={+limit} />
                ) : rows?.length ? (
                  rows
                ) : (
                  <TableEmpty columns={columns.length} />
                )}
              </tbody>
            </Table>
          </Paper>
          <TablePaginationBottom
            meta={users?.meta}
            page={page}
            setPage={setPage}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default UsersPage;
