import {
  ActionIcon,
  Box,
  Button,
  Container,
  createStyles,
  Group,
  Paper,
  Table,
  Title,
  Tooltip,
} from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import { useQueryClient } from "@tanstack/react-query";
import { useState } from "react";
import { Link } from "react-router-dom";
import { Edit, Trash } from "tabler-icons-react";
import DeleteConfirmationModal from "../../components/DeleteConfirmationModal";
import {
  TableEmpty,
  TableFilter,
  TablePaginationBottom,
  TablePaginationTop,
  TableSearch,
  TableSkeleton,
  Th,
} from "../../components/tables";
import ComponentGuard from "../../guards/ComponentGuard";
import { PERMISSIONS } from "../../guards/permissions";
import { ROLE_KEY, useRoles } from "../../services/roles/roles.hook";
import { deleteRole } from "../../services/roles/roles.service";

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors[theme.primaryColor][6],
    height: "100%",
  },
  titleContainer: {
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    height: 80,
    backgroundColor: theme.colors[theme.primaryColor][1],
  },
  bodyContainer: {
    height: "calc(100% - 80px)",
    padding: theme.spacing.lg,
  },
  title: {
    fontSize: theme.fontSizes.xl * 1.5,
  },
}));

const DEFAULT_SORT: Record<string, "NONE" | "ASC" | "DESC"> = {
  name: "NONE",
};

const RolesPage = () => {
  const { classes } = useStyles();
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState("5");
  const [search, setSearch] = useState("");
  const [filter, setFilter] = useState<Record<string, string>>({});
  const [sort, setSort] =
    useState<Record<string, "NONE" | "ASC" | "DESC">>(DEFAULT_SORT);
  const [deleteConfirmationModalOpen, setDeleteConfirmationModalOpen] =
    useState(false);
  const [deleteConfirmationModalData, setDeleteConfirmationModalData] =
    useState<{ id: number; name: string }>();
  const queryClient = useQueryClient();

  const sortQuery = Object.entries(sort)
    .filter((item) => item[1] !== "NONE")
    .map((item) => `${item[0]}:${item[1]}`)
    .join(",");

  const {
    data: roles,
    isLoading,
    isRefetching,
    refetch,
  } = useRoles({
    limit: limit === "All" ? undefined : +limit,
    page: page,
    search: search ? search : undefined,
    filter: filter,
    sortBy: sortQuery ? sortQuery : undefined,
  });

  const columns = [
    {
      name: "No.",
      field: "id",
      sortable: false,
    },
    {
      name: "Name",
      field: "name",
      sortable: true,
    },
    {
      name: "Display Name",
      field: "display_name",
      sortable: true,
    },
    // {
    //   name: "Description",
    //   field: "description",
    //   sortable: false,
    // },
    {
      name: "Action",
      field: "action",
      sortable: false,
    },
  ];

  const onSort = (field: string) => {
    const sortField = sort[field];
    if (sortField === "NONE") {
      setSort({ ...sort, [field]: "ASC" });
    } else if (sortField === "ASC") {
      setSort({ ...sort, [field]: "DESC" });
    } else {
      setSort({ ...sort, [field]: "NONE" });
    }
  };

  const isTableSorted = JSON.stringify(sort) !== JSON.stringify(DEFAULT_SORT);

  const handleDelete = async () => {
    try {
      await deleteRole(deleteConfirmationModalData?.id || 0);
      queryClient.invalidateQueries([ROLE_KEY]);
      showNotification({
        title: "Role deleted",
        message: "Role deleted successfully",
        color: "green",
      });
    } catch (error: any) {
      showNotification({
        title: "Error",
        message: error.message,
        color: "red",
      });
    }
  };

  const rows = roles?.data?.map((r, index) => (
    <tr key={r.id}>
      <td style={{ paddingLeft: 16 }}>
        {(roles.meta.currentPage - 1) * roles.meta.itemsPerPage + index + 1}
      </td>
      <td>{r.name}</td>
      <td>{r.display_name}</td>
      {/* <td>{r.description}</td> */}
      <td>
        <Group spacing={4}>
          <ComponentGuard permission={PERMISSIONS.ROLE.UPDATE}>
            <Tooltip label="Edit">
              <ActionIcon color="blue" component={Link} to={String(r.id)}>
                <Edit size={16} />
              </ActionIcon>
            </Tooltip>
          </ComponentGuard>
          <ComponentGuard permission={PERMISSIONS.ROLE.DELETE}>
            <Tooltip label="Delete">
              <ActionIcon
                color="red"
                onClick={() => {
                  setDeleteConfirmationModalOpen(true);
                  setDeleteConfirmationModalData({
                    id: r.id,
                    name: r.display_name,
                  });
                }}
              >
                <Trash size={16} />
              </ActionIcon>
            </Tooltip>
          </ComponentGuard>
        </Group>
      </td>
    </tr>
  ));

  return (
    <Box className={classes.root}>
      <DeleteConfirmationModal
        opened={deleteConfirmationModalOpen}
        setOpened={setDeleteConfirmationModalOpen}
        title="Delete Role"
        confirmationString={`Are you sure you want to delete "${deleteConfirmationModalData?.name}"?`}
        onConfirm={handleDelete}
      />
      <Group position="apart" align="center" className={classes.titleContainer}>
        <Title order={2} className={classes.title}>
          Roles
        </Title>
        <ComponentGuard permission={PERMISSIONS.ROLE.CREATE}>
          <Button component={Link} to="add">
            Add Role
          </Button>
        </ComponentGuard>
      </Group>
      <Box className={classes.bodyContainer}>
        <Box
          p="md"
          sx={{ backgroundColor: "white", borderRadius: 6, height: "100%" }}
        >
          <Paper withBorder mb="xs" p="sm">
            <Group position="apart">
              <Group align="flex-end">
                <TableSearch
                  search={search}
                  setSearch={setSearch}
                  placeholder="Name"
                  debounce={400}
                  onSearchChange={() => page !== 1 && setPage(1)}
                />
                <TableFilter
                  fields={[
                    {
                      name: "name",
                      displayName: "Name",
                      operator: "$eq",
                      value: {
                        type: "text",
                      },
                    },
                  ]}
                  filter={filter}
                  setFilter={setFilter}
                />
                <Group spacing="xs">
                  {isTableSorted && (
                    <Button
                      variant="subtle"
                      onClick={() => setSort(DEFAULT_SORT)}
                    >
                      Clear Sort
                    </Button>
                  )}
                </Group>
              </Group>
              <TablePaginationTop
                limit={limit}
                setLimit={setLimit}
                page={page}
                setPage={setPage}
                refetch={refetch}
                isRefetching={isRefetching}
                meta={roles?.meta}
              />
            </Group>
          </Paper>
          <Paper withBorder>
            <Table>
              <thead>
                <tr>
                  {columns.map((h) => (
                    <Th
                      key={h.field}
                      onSort={() => onSort(h.field)}
                      orderStatus={sort[h.field]}
                      sortable={h.sortable}
                    >
                      {h.name}
                    </Th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {isLoading ? (
                  <TableSkeleton columns={columns.length} rows={+limit} />
                ) : rows?.length ? (
                  rows
                ) : (
                  <TableEmpty columns={columns.length} />
                )}
              </tbody>
            </Table>
          </Paper>
          <TablePaginationBottom
            meta={roles?.meta}
            page={page}
            setPage={setPage}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default RolesPage;
