import {
  ActionIcon,
  Box,
  Button,
  Checkbox,
  createStyles,
  Group,
  Input,
  Paper,
  SimpleGrid,
  Stack,
  Text,
  Textarea,
  TextInput,
  Title,
  useMantineTheme,
} from "@mantine/core";
import { useInputState } from "@mantine/hooks";
import { showNotification } from "@mantine/notifications";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { ArrowLeft } from "tabler-icons-react";
import { useAvailablePermissions } from "../../services/roles/roles.hook";
import { createRole } from "../../services/roles/roles.service";

const useStyles = createStyles((theme) => ({
  root: {
    backgroundColor: theme.colors[theme.primaryColor][6],
    height: "100%",
  },
  titleContainer: {
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    height: 80,
    backgroundColor: theme.colors[theme.primaryColor][1],
  },
  bodyContainer: {
    height: "calc(100% - 80px)",
    padding: theme.spacing.lg,
  },
  title: {
    fontSize: theme.fontSizes.xl * 1.5,
  },
}));

const AddRolePage = () => {
  const navigate = useNavigate();
  const theme = useMantineTheme();
  const { classes } = useStyles();
  const [name, setName] = useInputState("");
  const [description, setDescription] = useInputState("");
  const [displayName, setDisplayName] = useInputState("");
  const [permissions, setPermissions] = useState<string[]>([]);

  const { data: dataAvailablePermissions } = useAvailablePermissions();
  const availablePermissions = dataAvailablePermissions?.data || [];

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const response = await createRole({
        name,
        description,
        display_name: displayName,
        permissions,
      });
      showNotification({
        title: "Role created",
        message: `Role ${response.data.name} created successfully`,
        color: "green",
      });
      navigate("/roles");
    } catch (error: any) {
      showNotification({
        title: "Error",
        message: error.message,
        color: "red",
      });
    }
  };

  return (
    <Box className={classes.root}>
      <Group align="center" className={classes.titleContainer}>
        <ActionIcon component={Link} to="/roles" color={theme.primaryColor}>
          <ArrowLeft />
        </ActionIcon>
        <Title order={2}>Add New Role</Title>
      </Group>

      <Box className={classes.bodyContainer}>
        <Paper
          withBorder
          p="md"
          sx={(theme) => ({ width: theme.breakpoints.sm })}
        >
          <form onSubmit={handleSubmit}>
            <Stack>
              <SimpleGrid cols={2}>
                <TextInput label="Name" value={name} onChange={setName} />
                <TextInput
                  label="Display Name"
                  value={displayName}
                  onChange={setDisplayName}
                />
              </SimpleGrid>
              <Textarea
                label="Description"
                value={description}
                onChange={setDescription}
              />
              <Checkbox.Group
                label="Permissions"
                value={permissions}
                onChange={(v) => {
                  const managedResources = v
                    .filter((p) => p.split(":")[0] === "manage")
                    .map((p) => p.split(":")[1]);

                  const filteredPermissions = v.filter((p) => {
                    return (
                      p.split(":")[0] === "manage" ||
                      !managedResources.includes(p.split(":")[1])
                    );
                  });

                  setPermissions(filteredPermissions);
                }}
              >
                <Paper withBorder p="sm" sx={{ width: "100%" }}>
                  <Stack>
                    {availablePermissions.map((group) => (
                      <div key={group.resource}>
                        <Title
                          order={4}
                          mb={4}
                          sx={(theme) => ({
                            fontSize: theme.fontSizes.md,
                            fontWeight: 500,
                            textTransform: "capitalize",
                          })}
                        >
                          {group.resource}
                        </Title>
                        <Group>
                          {group.permissions.map((permission) => (
                            <Checkbox
                              key={permission}
                              label={
                                <Text transform="capitalize">
                                  {permission.split(":")[0].replace("-", " ")}
                                </Text>
                              }
                              disabled={
                                permissions.includes(
                                  `manage:${permission.split(":")[1]}`
                                ) &&
                                permission !==
                                  `manage:${permission.split(":")[1]}`
                              }
                              value={permission}
                              checked={true}
                            />
                          ))}
                        </Group>
                      </div>
                    ))}
                  </Stack>
                </Paper>
              </Checkbox.Group>
            </Stack>
            <Group position="right" mt="lg">
              <Button type="submit">Submit</Button>
            </Group>
          </form>
        </Paper>
      </Box>
    </Box>
  );
};

export default AddRolePage;
