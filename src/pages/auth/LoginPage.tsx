import {
  Anchor,
  Button,
  Checkbox,
  Container,
  Group,
  LoadingOverlay,
  Paper,
  PasswordInput,
  Stack,
  Text,
  TextInput,
  Title,
} from "@mantine/core";
import { useForm, zodResolver } from "@mantine/form";
import { showNotification } from "@mantine/notifications";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { z } from "zod";
import { useAuth } from "../../contexts/AuthContext";

const schema = z.object({
  email: z.string().email(),
  password: z.string(),
});

const LoginPage = () => {
  const navigate = useNavigate();
  const { login } = useAuth();
  const [submitting, setSubmitting] = useState(false);

  const form = useForm({
    validate: zodResolver(schema),
    initialValues: {
      email: "",
      password: "",
    },
  });

  const handleSubmit = async (values: typeof form.values) => {
    try {
      setSubmitting(true);
      await login({ email: values.email, password: values.password });
      setSubmitting(false);
      showNotification({
        title: "Login Success",
        message: "You have successfully logged in",
        color: "green",
      });
      navigate("/");
    } catch (error) {
      setSubmitting(false);
      showNotification({
        title: "Login Failed",
        message: "Please check your email and password",
        color: "red",
      });
    }
  };

  return (
    <Container size={420} my={40}>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Paper
          withBorder
          shadow="md"
          p={30}
          mt={30}
          radius="md"
          style={{ position: "relative" }}
        >
          <Group position="center">
            {/* <Image src={logo} width={180} ml={-15} /> */}
            <Title>Logo</Title>
          </Group>
          <Title
            mt="sm"
            mb="xl"
            align="center"
            sx={(theme) => ({
              fontWeight: 700,
              fontSize: theme.fontSizes.xl * 1.2,
            })}
          >
            Welcome back
          </Title>
          <LoadingOverlay visible={submitting} />
          <Stack spacing="sm">
            <TextInput
              required
              label="Email"
              placeholder="you@mantine.dev"
              {...form.getInputProps("email")}
            />
            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              {...form.getInputProps("password")}
            />
          </Stack>
          <Group position="apart" mt="md">
            <Checkbox label="Remember me" />
            <Anchor<"a">
              onClick={(event) => event.preventDefault()}
              href="#"
              size="sm"
            >
              Forgot Password
            </Anchor>
          </Group>
          <Button type="submit" fullWidth mt="xl">
            Login
          </Button>
          <Text size="sm" mt="md">
            Don't have an account?{" "}
            <Anchor component={Link} to="/signup">
              Sign up
            </Anchor>
          </Text>
        </Paper>
      </form>
    </Container>
  );
};

export default LoginPage;
