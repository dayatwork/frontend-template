import {
  Anchor,
  Button,
  Checkbox,
  Container,
  Group,
  LoadingOverlay,
  Paper,
  PasswordInput,
  Stack,
  Text,
  TextInput,
  Title,
} from "@mantine/core";
import { useForm, zodResolver } from "@mantine/form";
import { showNotification } from "@mantine/notifications";
import { AxiosError } from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { z } from "zod";
import { useAuth } from "../../contexts/AuthContext";

const schema = z.object({
  name: z.string(),
  email: z.string().email(),
  phone: z.string(),
  password: z.string(),
});

const SignupPage = () => {
  const navigate = useNavigate();
  const { signup } = useAuth();
  const [submitting, setSubmitting] = useState(false);

  const form = useForm({
    validate: zodResolver(schema),
    initialValues: {
      name: "",
      email: "",
      phone: "",
      password: "",
    },
  });

  const handleSubmit = async (values: typeof form.values) => {
    try {
      setSubmitting(true);
      await signup({
        name: values.name,
        email: values.email,
        phone: values.phone,
        password: values.password,
      });
      setSubmitting(false);
      showNotification({
        title: "Sign Up Success",
        message: "You have successfully signed up",
        color: "green",
      });
      navigate("/");
    } catch (error) {
      setSubmitting(false);
      let message = "Please check your email and password";
      if (error instanceof AxiosError) {
        if (typeof error.response?.data?.message === "string") {
          message = error.response?.data?.message;
        } else {
          message = error.response?.data?.message?.join(", ");
        }
      }
      showNotification({
        title: "Sign Up Failed",
        message: message,
        color: "red",
      });
    }
  };

  return (
    <Container size={420} my={40}>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Paper
          withBorder
          shadow="md"
          p={30}
          mt={30}
          radius="md"
          style={{ position: "relative" }}
        >
          <Group position="center">
            {/* <Image src={logo} width={180} ml={-15} /> */}
            <Title>Logo</Title>
          </Group>
          <Title
            mt="sm"
            mb="xl"
            align="center"
            sx={(theme) => ({
              fontWeight: 700,
              fontSize: theme.fontSizes.xl * 1.2,
            })}
          >
            Create Account
          </Title>
          <LoadingOverlay visible={submitting} />
          <Stack spacing="sm">
            <TextInput
              required
              label="Name"
              placeholder="your name"
              {...form.getInputProps("name")}
            />
            <TextInput
              required
              label="Email"
              placeholder="you@mantine.dev"
              {...form.getInputProps("email")}
            />
            <TextInput
              required
              label="Phone"
              placeholder="+6281234567890"
              {...form.getInputProps("phone")}
            />
            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              {...form.getInputProps("password")}
            />
          </Stack>
          <Group position="apart" mt="md">
            <Checkbox label="Remember me" />
            <Anchor<"a">
              onClick={(event) => event.preventDefault()}
              href="#"
              size="sm"
            >
              Forgot Password
            </Anchor>
          </Group>
          <Button type="submit" fullWidth mt="xl">
            Sign up
          </Button>
          <Text size="sm" mt="md">
            Already have an account?{" "}
            <Anchor component={Link} to="/login">
              Login
            </Anchor>
          </Text>
        </Paper>
      </form>
    </Container>
  );
};

export default SignupPage;
