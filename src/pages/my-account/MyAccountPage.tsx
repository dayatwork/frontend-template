import {
  Avatar,
  Button,
  createStyles,
  Group,
  LoadingOverlay,
  Paper,
  PasswordInput,
  SimpleGrid,
  Stack,
  TextInput,
  Title,
} from "@mantine/core";
import React, { useState } from "react";
import { useAuth } from "../../contexts/AuthContext";
import avatar from "../../assets/avatar.png";
import { useInputState } from "@mantine/hooks";
import { changePassword } from "../../services/auth/auth.service";
import { showNotification } from "@mantine/notifications";

const useStyles = createStyles((theme) => ({
  root: {},
  title: {
    fontSize: theme.fontSizes.xl * 1.2,
  },
  subtitle: {
    fontSize: theme.fontSizes.xl,
    fontWeight: 500,
  },
}));

const MyAccountPage = () => {
  const { classes } = useStyles();
  const { user } = useAuth();
  const [password, setPassword] = useInputState("");
  const [newPassword, setNewPassword] = useInputState("");
  const [changingPassword, setChangingPassword] = useState(false);

  const handleChangePassword = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      setChangingPassword(true);
      await changePassword({
        current_password: password,
        new_password: newPassword,
      });
      showNotification({
        title: "Success",
        message: "Password changed successfully",
        color: "green",
      });
      setChangingPassword(false);
      setPassword("");
      setNewPassword("");
    } catch (error: any) {
      showNotification({
        title: "Error",
        message: error.response?.data?.message,
        color: "red",
      });
      setChangingPassword(false);
    }
  };

  return (
    <>
      <Title mb="lg" className={classes.title}>
        My Account
      </Title>

      <Paper
        withBorder
        p="md"
        sx={(theme) => ({ maxWidth: theme.breakpoints.sm })}
      >
        <Title mb="md" order={3} className={classes.subtitle}>
          Personal Info
        </Title>
        <Group noWrap align="flex-start" spacing="xl">
          <Avatar mt={8} size={120} src={avatar} />
          <Stack sx={{ flex: 1 }}>
            <TextInput readOnly label="Name" value={user?.name} />
            <TextInput readOnly label="Email" value={user?.email} />
            <TextInput readOnly label="Phone" value={user?.phone} />
          </Stack>
        </Group>
      </Paper>
      <Paper
        mt="lg"
        withBorder
        p="md"
        sx={(theme) => ({ maxWidth: theme.breakpoints.sm })}
      >
        <Title mb="md" order={3} className={classes.subtitle}>
          Change Password
        </Title>
        <form onSubmit={handleChangePassword} style={{ position: "relative" }}>
          <LoadingOverlay visible={changingPassword} />
          <SimpleGrid cols={2}>
            <PasswordInput
              label="Current Password"
              value={password}
              onChange={setPassword}
            />
            <PasswordInput
              label="New Password"
              value={newPassword}
              onChange={setNewPassword}
            />
          </SimpleGrid>
          <Group mt="lg" position="right">
            <Button type="submit" disabled={!password || !newPassword}>
              Change
            </Button>
          </Group>
        </form>
      </Paper>
    </>
  );
};

export default MyAccountPage;
