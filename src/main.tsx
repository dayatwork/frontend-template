import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";

import App from "./App";
import "./i18n";
import {
  ColorScheme,
  ColorSchemeProvider,
  MantineProvider,
} from "@mantine/core";
import { NotificationsProvider } from "@mantine/notifications";
import { AuthProvider } from "./contexts/AuthContext";
import { useHotkeys, useLocalStorage } from "@mantine/hooks";
import { FetchProvider } from "./contexts/FetchContext";

const client = new QueryClient();

const AppWrapper = () => {
  const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
    key: "mantine-color-scheme",
    defaultValue: "light",
    getInitialValueInEffect: true,
  });

  const toggleColorScheme = (value?: ColorScheme) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));

  useHotkeys([["mod+J", () => toggleColorScheme()]]);

  return (
    <ColorSchemeProvider
      colorScheme={colorScheme}
      toggleColorScheme={toggleColorScheme}
    >
      <MantineProvider
        theme={{
          fontFamily: "Inter var, sans-serif",
          colors: {
            brand: [
              "#FBF7EE",
              "#F2E7CD",
              "#F2E7CD",
              "#EEDAA1",
              "#E2C88C",
              "#DAB96B",
              "#B39959",
              "#7D611F",
              "#503F14",
              "#513F14",
            ],
          },
          primaryColor: "brand",
        }}
        withGlobalStyles
        withNormalizeCSS
      >
        <NotificationsProvider position="top-right">
          <App />
        </NotificationsProvider>
      </MantineProvider>
    </ColorSchemeProvider>
  );
};

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Suspense fallback="Loading...">
      <AuthProvider>
        <FetchProvider>
          <BrowserRouter>
            <QueryClientProvider client={client}>
              <AppWrapper />
            </QueryClientProvider>
          </BrowserRouter>
        </FetchProvider>
      </AuthProvider>
    </Suspense>
  </React.StrictMode>
);
