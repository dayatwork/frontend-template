// import axios from "../axios";
import { authAxios as axios } from "../../contexts/FetchContext";
import { LinksType, MetaType } from "../types";

type Role = {
  id: number;
  name: string;
  display_name: string;
};

export type User = {
  id: string;
  name: string;
  email: string;
  phone: string;
  roles: Role[];
};

// =======================
// ====== Get Users ======
// =======================
export type GetUsersParams = {
  limit?: number;
  page?: number;
  sortBy?: string;
  search?: string;
  filter?: { [key: string]: string };
};

export type GetUsersReponse = {
  statusCode: number;
  data: User[];
  meta: MetaType;
  links: LinksType;
};

export const getUsers = async (
  params: GetUsersParams,
  signal: AbortSignal | undefined
) => {
  const response = await axios.get<GetUsersReponse>("/users", {
    params,
    signal,
  });
  return response.data;
};

// =======================
// ====== Get User =======
// =======================
export type GetUserParams = {
  id: string;
};

export type GetUserReponse = {
  statusCode: number;
  data: User;
};

export const getUser = async (
  params: GetUserParams,
  signal: AbortSignal | undefined
) => {
  const response = await axios.get<GetUserReponse>(`/users/${params.id}`, {
    signal,
  });
  return response.data;
};

// =============================
// ====== Set User Roles =======
// =============================
export type SetUserRolesData = {
  userId: string;
  rolesIds: number[];
};

export type SetUserRoleResponse = {
  statusCode: number;
  data: User;
};

export const setUserRoles = async (data: SetUserRolesData) => {
  const response = await axios.post<SetUserRoleResponse>(`/users/roles`, data);
  return response.data;
};
