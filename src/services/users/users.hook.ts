import { useQuery } from "@tanstack/react-query";

import { getUser, getUsers, GetUsersParams } from "./users.service";

export const USER_KEY = "users";

export const useUsers = (params: GetUsersParams) => {
  return useQuery([USER_KEY, params], ({ signal }) => getUsers(params, signal));
};

export const useUser = (id: string) => {
  return useQuery([USER_KEY, id], ({ signal }) => getUser({ id }, signal));
};
