// import axios from "../axios";
import { authAxios as axios } from "../../contexts/FetchContext";
import { LinksType, MetaType } from "../types";

export type Role = {
  id: number;
  name: string;
  display_name: string;
  description?: string;
  permissions: string[];
};

// =======================
// ====== Get Roles ======
// =======================
export type GetRolesParams = {
  limit?: number;
  page?: number;
  sortBy?: string;
  search?: string;
  filter?: { [key: string]: string };
};

export type GetRolesReponse = {
  statusCode: number;
  data: Role[];
  meta: MetaType;
  links: LinksType;
};

export const getRoles = async (
  params: GetRolesParams,
  signal: AbortSignal | undefined
) => {
  const response = await axios.get<GetRolesReponse>("/roles", {
    params,
    signal,
  });
  return response.data;
};

// =======================
// ====== Get Role =======
// =======================
export type GetRoleParams = {
  id: string;
};

export type GetRoleReponse = {
  statusCode: number;
  data: Role;
};

export const getRole = async (
  params: GetRoleParams,
  signal: AbortSignal | undefined
) => {
  const response = await axios.get<GetRoleReponse>(`/roles/${params.id}`, {
    signal,
  });
  return response.data;
};

// ==========================
// ====== Create Role =======
// ==========================
export type CreateRolePayload = {
  name: string;
  display_name: string;
  description?: string;
  permissions: string[];
};

export type CreateRoleReponse = {
  statusCode: number;
  data: Role;
};

export const createRole = async (payload: CreateRolePayload) => {
  const response = await axios.post<CreateRoleReponse>("/roles", payload);
  return response.data;
};

// ========================
// ====== Edit Role =======
// ========================
export type EditRolePayload = {
  name: string;
  display_name: string;
  description?: string;
  permissions: string[];
};

export type EditRoleReponse = {
  statusCode: number;
  data: Role;
};

export const editRole = async (id: number, payload: EditRolePayload) => {
  const response = await axios.patch<EditRoleReponse>(`/roles/${id}`, payload);
  return response.data;
};

// ==========================
// ====== Delete Role =======
// ==========================
export type DeleteRoleReponse = {
  statusCode: number;
};

export const deleteRole = async (id: number) => {
  const response = await axios.delete<DeleteRoleReponse>(`/roles/${id}`);
  return response.data;
};

// ========================================
// ====== Get Available Permissions =======
// ========================================
export type GetAvailablePermissionsResponse = {
  statusCode: number;
  data: {
    resource: string;
    permissions: string[];
  }[];
};

export const getAvailablePermissions = async (
  signal: AbortSignal | undefined
) => {
  const response = await axios.get<GetAvailablePermissionsResponse>(
    "/roles/available-permissions",
    {
      signal,
    }
  );
  return response.data;
};
