import { useQuery } from "@tanstack/react-query";

import {
  getAvailablePermissions,
  getRole,
  getRoles,
  GetRolesParams,
} from "./roles.service";

export const ROLE_KEY = "roles";

export const useRoles = (params: GetRolesParams) => {
  return useQuery(
    [ROLE_KEY, params],
    ({ signal }) => getRoles(params, signal),
    { staleTime: Infinity }
  );
};

export const useRole = (id: string) => {
  return useQuery([ROLE_KEY, id], ({ signal }) => getRole({ id }, signal), {
    enabled: Boolean(id),
  });
};

export const AVAILABLE_PERMISSION_KEY = "available-permissions";

export const useAvailablePermissions = () => {
  return useQuery(
    [AVAILABLE_PERMISSION_KEY],
    ({ signal }) => getAvailablePermissions(signal),
    { staleTime: Infinity }
  );
};
