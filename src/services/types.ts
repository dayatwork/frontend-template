export type MetaType = {
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  totalPages: number;
  sortBy: [string, "ASC" | "DESC"][];
  search?: string;
  searchBy?: string[];
  filter?: Record<string, string>;
};

export type LinksType = {
  first?: string;
  previous?: string;
  current: string;
  next?: string;
  last?: string;
};
