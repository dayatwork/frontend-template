import axios from "../axios";
import { authAxios } from "../../contexts/FetchContext";

type Role = {
  id: number;
  name: string;
};

export type User = {
  id: string;
  name: string;
  email: string;
  phone: string;
  expiration_date: string | null;
  coins: number;
  roles: Role[];
  permissions: string[];
};

// ===================
// ====== Signup ======
// ===================
export type SignupData = {
  name: string;
  email: string;
  phone: string;
  password: string;
};

export type SignupReponse = {
  statusCode: number;
  data: {
    accessToken: string;
    refreshToken: string;
    user: User;
  };
};

export const signup = async (data: SignupData) => {
  const response = await axios.post<SignupReponse>("/auth/signup", data);
  return response.data;
};

// ===================
// ====== Login ======
// ===================
export type LoginData = {
  email: string;
  password: string;
};

export type LoginReponse = {
  statusCode: number;
  data: {
    accessToken: string;
    refreshToken: string;
    user: User;
  };
};

export const login = async (data: LoginData) => {
  const response = await axios.post<LoginReponse>("/auth/login", data);
  return response.data;
};

// ===================
// ======== Me =======
// ===================
export type MeReponse = {
  statusCode: number;
  data: User;
};
export const me = async () => {
  const response = await authAxios.get<MeReponse>("/auth/me");
  return response.data;
};

// ===================
// ====== Logout =====
// ===================
export type LogoutReponse = {
  statusCode: number;
  data: {
    message: string;
  };
};
export const logout = async () => {
  const response = await axios.post<LogoutReponse>("/auth/logout");
  return response.data;
};

// ===================
// ====== Clear ======
// ===================
export type ClearReponse = {
  statusCode: number;
  data: {
    message: string;
  };
};
export const clear = async () => {
  const response = await axios.post<ClearReponse>("/auth/clear");
  return response.data;
};

// ===================
// ===== Refresh =====
// ===================
export type RefreshReponse = {
  statusCode: number;
  data: {
    message: string;
  };
};
export const refresh = async () => {
  const response = await axios.post<RefreshReponse>("/auth/refresh");
  return response.data;
};

// ===========================
// ===== Change Password =====
// ===========================
export type ChangePasswordData = {
  current_password: string;
  new_password: string;
};

export type ChangePasswordReponse = {
  statusCode: number;
  data: {
    accessToken: string;
    refreshToken: string;
    user: User;
  };
};

export const changePassword = async (data: ChangePasswordData) => {
  const response = await authAxios.post<ChangePasswordReponse>(
    "/auth/change-password",
    data
  );
  return response.data;
};
